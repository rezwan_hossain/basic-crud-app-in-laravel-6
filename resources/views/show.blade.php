<div>
	<h1>{{ $todos->title }}</h1>
	<h3>{{ $todos->description }}</h3>

	<a href="/todos/{{ $todos->id }}/edit">edit</a>
	<div>
		<form method="POST" action="/todos/{{ $todos->id }}">
			@csrf
			@method('DELETE')

			<button type="submit">delete</button>
		</form>
	</div>
</div>