<div>
	<form method="POST" action="/todos/{{ $todos->id }}">
		@csrf
		@method('PUT')
		<input type="text" name="title" placeholder="title" value=" {{ $todos->title }} ">
		<textarea name="description" placeholder="description" > {{ $todos->description }} </textarea>
		<button type="submit">update</button>
	</form>
</div>