


# Basic CRUD app in laravel 6


## MVC  Structure

 - **Models** represents the entities in database and helps you to query the database and return the data
 -    **Views**  are the pages which will be displayed when accessed the app. View Component is used for the User Interface of the application.
 - **Controllers** handle user requests, gets required data from the models and pass them to the Views. Controllers acts as an intermediary between Model and View Components to process the business logic and incoming request.
 

##  Install Laravel

use laravel 6 with using the following command (Make sure you have installed composer in your PC)

    composer create-project --prefer-dist laravel/laravel Todo

use spacific version of laravel   using the following command (Make sure you have installed composer in your PC)

    composer create-project --prefer-dist laravel/laravel="7.0.*" Todo


## Configure Database
You need to create the database at MySQL, and then we need to connect that database to the Laravel project. Open the .env file inside the Laravel project and add the database credentials as below.
    
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=Your DB Name
    DB_USERNAME=Your DB UserName
    DB_PASSWORD=Your DB Password


## Create Controller
Instead of defining all of your request handling logic as Closures in route files, you may wish to organize this behavior using Controller classes. Controllers can group related request handling logic into a single class. Controllers are stored in the  `app/Http/Controllers`  directory.

create the **TodoController** using the following command

    php artisan make:controller TodoController

Our controller look like this 

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TodosControllerextends Controller
{
    //App logic right here
}

```

## Seven Restful Controller Action

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class TodoController extends Controller
{
    public function index()
    {
    	// show all the resourse
    }
    public function show($id)
    {
    	// show single resoures
    }
    public function create()
    {
    	// Show a view to create new resourse
    }
    public function store()
    {
    	// persist new resourse
    }
    public function edit($id)
    {
    	// Show a view to edit existing resourse
    }
    public function update($id)
    {
    	// persist edited resourse
    }
    public function destroy($id)
    {
    	// Delete the resourse
    }
}
```
We can generate controller with  Seven Restful Controller with following command 

    php artisan make:controller "controllerName" --resource

## Create Model
In MVC framework, the letter `M`stands for Model. Model are means to handle the business logic in any MVC framework based application. In Laravel, Model is a class that represents the logical structure and relationship of underlying data table. In Laravel, each of the database table has a corresponding “Model” that allow us to interact with that table. Models gives you the way to retrieve, insert, and update information into your data table.All of the Laravel Models are stored in the main

Type the following command to create a model

    php artisan make:model Todo
   In laravel, the name of a model has to be singular, and the name of the migration should be the plural so it can automatically find a table name.

>You can make **model** + **migration** + **controller**, all in one line, using following command

    php artisan make:model "Modelname" -mc
   

 - -m = migration
 - -c =contorller     
 >You can make **model** + **migration** + **controller**, all in one line, using following command

    php artisan make:model "Modelname" -mcr
   

 - -m = migration
 - -c = resourceful contorller     
> You can make  **model** + **migration** + **factory** + **controller** for the model

    php artisan make:model "Modelname" -a

## Create the migration file
Migrations are like version control for your database, allowing your team to modify and share the application's database schema. Migrations are typically paired with Laravel's schema builder to build your application's database schema. If you have ever had to tell a teammate to manually add a column to their local database schema, you've faced the problem that database migrations solve.

Go to the terminal and type the following php artisan command to generate the model and migration file.

    php artisan make:migration create_todos_table --create=todos

The migration will be created under "database/migrations". Edit the file with the code below to create Todos Table.


```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}    
```
Now run the following command migrate database schema 

    php artisan migrate

 

##  Defining Routes
**Route** is a way of creating a request URL of your application. These URL do not have to map to specific files on a website

Our route should  look like following  table 

|Method                |URI        |Action|
|----------------|-------------------------------|-----------------------------|
|GET|/           |App\Http\Controllers\TodoController@index|
|GET|todos/create|App\Http\Controllers\TodoController@create|
|POST|todos      |App\Http\Controllers\TodoController@store|
|GET|todos/{todo}|App\Http\Controllers\TodoController@show|
|GET|todos/{todo}/edit|App\Http\Controllers\TodoController@edit|
|PUT|todos/{todo}|App\Http\Controllers\TodoController@update|
|DELETE|todos/{todo}|App\Http\Controllers\TodoController@destroy|

Our rote file is 	`routes/web.php`
```php
<?php

/*
|---------------------------------------------------------------------
| Web Routes
|---------------------------------------------------------------------
|
| Here is where you can register web routes for your application.These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','TodoController@index');
Route::get('/todos/create', 'TodoController@create');
Route::post('/todos', 'TodoController@store')->name('todos.store');
Route::get('/todos/{todo}','TodoController@show')->name('todos.show');
Route::get('/todos/{todo}/edit','TodoController@edit');
Route::put('/todos/{todo}', 'TodoController@update');
Route::delete('/todos/{todo}','TodoController@destroy');


/*
|-------------------------------------------------------------------
| if we use standard convention resourceful route we can write in
| single line of code like below
|-------------------------------------------------------------------
*/ 
Route::resource('todos','TodoController');
```
## Frontend scaffolding


In laravel 5 we could use bootstrap, jquery right out of the box but in laravel 6
all things move to laravel UI. To use laravel UI, we need to download the package via composer with the following command. 

    composer require laravel/ui
    
Laravel  UI package now  use auth command 

    php artisan ui:auth     //in laravel 5 make:auth

Now if we want laravel bootstrap or vue or react  support we can use

    php artisan ui bootstrap
    
> we can add   --auth flag to the command 

> this tutorial I am not going to use any bootstrap or any CSS it's just plain HTML

## Index page

in this page, we want to  show all the data form database 

we need to create router first and this route will be our root route
so our route should look like these
```php
Route::get('/','TodoController@index');
```
now in the controller we need to create index function 

```php
public function index()
{
	// show all the resorse
}
```
We already crate database and migration, and our table has `title` and `description`. now we are going to add some dummy data to our database using 
`Table Plus` or `tinker`
Now we have some data in our database is time to render the data in our index page  
in laravel when we are working on the database, we need to work with the `model`,
to use the model, we need to import the database model.
 We already created model name **`Todo`** its time import the model. 

```php
use App\Todo;
```
Now we imported the model now we can access our database 
```php
public function index()
    {
    	//show all the resourse
    	$todos = Todo::all(); // Todo is the model name
    	return view('index', ['todos'=>$todos]);
	    //or
	    return view('index',compact('todos'));
	    //or
	    return view('index')->with('todos', $todos);
    }
```
> **[Named Routes](https://laravel.com/docs/6.x/routing#named-routes)**
Named routes allow the convenient generation of URLs or redirects for specific routes. You may specify a name for a route by chaining the `name` method onto the route definition:

> `compact()`; just used to take some variables as a parameter and return the output array with the variables.!  
Laravel uses it to pass variables from **controller** to its **View**, but there are other several ways for doing it too, you can not stuck to just one `compact();` 
>you can use  **Laravel**'s way of passing data to  **Views**, is by using either
`with('value'=>  'key'); 

access all data from model we use `all()`.
`$todos = Todo::all();` this line mean lets fetch all the data from **`Todo`** model and store `$todos` variable  and 
`return view('index', ['todos'=>$todos]);` return the index page with pass   some `$todos` variable data .  

in **resourses/views** folder create "**index.blade.php**" file.
```html
<div>
    <div>
        ToDo
    </div>
    <div>
        <a href="/todos/create">create Todo</a>
    </div>
</div>
<div>
    @foreach($todos as $todo)
        <a href="/todos/{{$todo->id}}">{{$todo->name}}</a>
    @endforeach
 </div>
```

we write it two-section, first section `anchor` or `<a>` tag that defines a hyperlink to another route
second section, when we pass `$todos` variable with data,  now we can use this variable. now we can use a loop to render each data from the model/database. **`foreach`**used for looping through the values of an array. Our `$todos` is an array object. how `foreach` work  is shown below
>`foreach(array  as variable)` so what we are saying is  for each item of an `array` pass it to the `variable` that we can use each item,

we are creating a `<a>`with a `wildcard/dynamic` link to another route an we give `<a>` name from the database/model .

```php
Route::get('/','TodoController@index')->name('todos.index');
```

```php
<div>
    <div>
        ToDo
    </div>
    <div>
        <a href="/todos/create">create Todo</a>
        or 
        <a href="{{ route('todos.create') }}">create Todo</a>
    </div>
</div>
<div>
    @foreach($todos as $todo)
        <a href="/todo/{{$todo->id}}">{{$todo->name}}</a>
        or
        <a href="{{ route('todos.create', $todo->id) }}">{{$todo->name}}</a>
    @endforeach
 </div>
```

## Working with form and POST method

earlier in this tutorial, we manually created data using `table plus` or `tinker`
now we create a form for our todo app 

 we create route for our app that can show our form. 
```php
Route::get('/todos/create', 'TodoController@create');
```
In the controller  we create a `create` method
```php
public function create()
    {
    	// Show a view to create new resourse
    	return view('create');
    }
```

in **resourses/views** folder create "**create.blade.php**" file. Now make basic html form
```html
<div>
    <form method="" action="">
		@csrf
        <input name="title" placeholder="title" type="text">
        <textarea name="description" placeholder="description">
        </textarea>
        <button type="submit">
            submit
        </button>
    </form>
</div>
```
In our form we add `@csrf` keyword for  **CSRF Protection**
> **[CSRF Protection](https://laravel.com/docs/6.x/csrf)**
Any HTML forms pointing to  `POST`,  `PUT`, or  `DELETE`  routes that are defined in the  `web`  routes file should include a **@csrf** token field. Otherwise, the request will be rejected

In our form we have specify method name if use hit submit button and we have specify action  that what route it our app go  after user click submit button 

In the form our method will be `POST` method , if we want to persist data in the database we will use `POST` method for data persistence,  as for the action we  need to create `post` route in our router 
```php
Route::post('/todos', 'TodoController@store')->name('todos.store');
```
and our html form look like this 
```html
<div>
    <form method="POST" action="/todos">
		@csrf
        <input name="title" placeholder="title" type="text">
        <textarea name="description" placeholder="description">
        </textarea>
        <button type="submit">
            submit
        </button>
    </form>
</div>
```
if we give a name to route. we can use given name route anywhere
```html
<!-- To This -->
<form  method="POST"  action="/todos">

<!-- This -->
<form method="POST" action="{{rotue('todos.store')}}">		
```
If we click the `submit` button it will throw an error, we need to process the request. `TodoController@store` we will add some functionality
```php
public function store(Request $request)
    {
    	// persist new resourse
    	$todos = new Todo;
    	$todos ->title= $request->title;
    	$todos ->description = $request->description;
    	$todos ->save();
		// or
		$todos = new Todo;
    	$todos ->title = request('title');
    	$todos ->description = request('description');;
    	$todos ->save();
    	
    	return redirect('/');
    }
```
`store` method is responsible for persisting new resource our case todo data,  how do we fetch the data ?  its on the `request()` helper or `Request $request` . request() is just a helper function that returns the same Request object.
request() can be called from anywhere within your app without the need to reference the full namespace (Illuminate\Http\Request). 
We instantiate a new `Todo` model and assign the`$todos` variable. Our model has two field one `title` and other `description`

## Show the single  resource
its time for fetch single  data from database and show it
we create a new router to render our show page 
```php
Route:get('/todos/{todo}','TodoController@show')
```
in **resourses/views** folder create "**show.blade.php**" file 

now we created `show` method in our controller 
```php
public funtction show (){
	// show single resources
}
```
So we want to visit single resources, so we have uniquely identified that resource.
We can identify the resource by their `id`. Our router accepts a `wildcard`. We have to pass the `id` to our `wildcard`.
In our `index` page we already send the `id` of our resources, we have to do find the resources by the `id`.

in the controller  we can pass that `id`  in the show method 
```php
public funtction show ($id){ //$id is variable 
	// show single resources  
	$todos = Todo::find($id); // Todo is model name
    	
    return view('show', ['todos'=>$todos]);
    //or
    return view('show',compact('todos'));
    //or
    return view('show')->with('todos', $todos);
}
```
we use `find()` to find our resources  and assign to a variable and we pass
the variable to `show` page to use that data .
```html
	<div>
		<h1>{{ $todos->title}}</h1>
		<h3>{{$todos->description}}</h3>
	</div>
	<a href="/todos/{{$todos->id}}/edit">Edit</a>
	<!-- we will write delete code later -->
	<a href="/todos/{{$todos->id}}/delete">Delete</a> 
```


## Update and 'PUT' request

Our current app we cant update our data if some thing need to change we cant do it  so we need to create update in our app  

now we are create router for showing  update form
```php
Route::get('/todos/{todo}/edit','TodoController@edit');
```
**resourses/views**  folder create “**edit.blade.php**” file. Now make basic html form

In the controller we create `edit` method 
```php
public function edit ()
{
    // Show a view to edit existing resources
}
```
`edit` method same as the `show` method, we edit our existing resources. To edit 
existing resources we have to find it first so as the `show` method we find our 
resources by `id`  and pass it to the `edit` page so that the `edit` page can see the existing resources

In the controller
```php
public function edit ($id)
{
    // Show a view to edit existing resourse
    $todos = Todo::find($id); // Todo is model name
    	
	return view('edit', ['todos'=>$todos]);
	//or
	return view('edit',compact('todos'));
	//or
	return view('edit')->with('todos', $todos);
    	
}
```
our frontend should look like `create`  from
```html
<div class="">
    <form method="POST" action="/todos/">
		@csrf
        <input name="title" placeholder="title" type="text">
        <textarea name="description" placeholder="description">
        </textarea>
        <button type="submit">
            submit
        </button>
    </form>
</div>
```
our frontend look like `create` page but we didn't create new resources, we are 
editing our existing resources so we need to change our form little bit

```html
<div class="">
    <form method="POST" action="/todos/{{ $todos->id }}">
	  @csrf
	  @method('PUT')
      <input name="title" placeholder="title" type="text" value="{{$todos->title}}">
      <textarea name="description" placeholder="description">
        {{ $todos->description }}
      </textarea>
      <button type="submit">
          submit
      </button>
    </form>
</div>
```
We can use the `PUT` method for editing any resources, but the problem in our browser doesn't understand the `PUT` method, so we need to make it understand.
So we add `@method('PUT')` to understand the browser. So what happens is laravel is telling the browser, listen to the browser I am using `POST` request but its not really a `POST` request but its really a `PUT` request for editing existing
resources. Our action route should be  

If we click the submit button it will throw a error because we did not  create `update` route .

```php
Route::put('/todos/{todo}','TodoController@update');
```
Our `update` method should look like `store` method
```php
public function update(Request $request, $id)
{
    // persist edited resourse
    $todos = Todo::find($id);
    
    $todos ->title = $request->title;
    $todos ->description = $request->description;
    $todos ->save();
	// or
    $todos ->title= request('title');
    $todos ->description = request('description');;
    $todos ->save();
    	
    return redirect('/todos/'. $todos->id);
    // or
    return redirect()->route('todos.show', $todos->id);
    	
}
```
We r not store the resources we are updating our existing resources, we need to find the existing data that we want to update  so pass `$id` to our update controller now we can update our existing resources

## Delete request

```php
Route::delete('/todos/{todo}','TodoController@destroy');
```

```php
public function destroy($id)
{
	// Delete the resourse
    $todo = Todo::find($id);
    $todo->delete();
    
    return redirect('/');
}
```
if want to delete any resources  we need to find the resources by the `$id` than we can use `delete()` method 

```html
<form method="POST" action="/todo/{{$todos->id}}">
	@csrf
	@method('DELETE')
	<button type="submit">Delete</button>
</form>
or
<form method="POST" action="{{route('todos.destroy',$todos->id)}}">
	@csrf
	@method('DELETE')
	<button type="submit">Delete</button>
</form>
```




# Follow Laravel naming conventions

|   What   |How |Good| bad
|---------|----|---------|-----------|
|Controller|singular |ArticleController|~~ArticlesController~~
|Route | plural | articles/1 | ~~article/1~~
|Named route | snake_case with dot notation | users.show_active | ~~users.show-active, show-active-users~~
|Model | singular | User | ~~Users~~
|hasOne or belongsTo relationship | singular | articleComment | ~~articleComments, article_comment~~
|All other relationships | plural | articleComments | ~~articleComment, article_comments~~
|Table | plural | article_comments | ~~article_comment, articleComments~~
|Pivot table | singular model names in alphabetical order | article_user | ~~user_article, articles_users~~
|Table column | snake_case without model name | meta_title | ~~MetaTitle; article_meta_title~~
|Model property | snake_case | $model->created_at | ~~$model->createdAt~~
|Foreign key | singular model name with _id suffix | article_id | ~~ArticleId, id_article, articles_id~~
|Primary key | - | id | ~~custom_id~~
|Method in resource controller | [table](https://laravel.com/docs/master/controllers#resource-controllers) | store | ~~saveArticle~~
|Variable | camelCase | $articlesWithAuthor | ~~$articles_with_author~~
|Collection | descriptive, plural | $activeUsers = User::active()->get() | ~~$active, $data~~
|Object | descriptive, singular | $activeUser = User::active()->first() | ~~$users, $obj~~
|View | kebab-case | show-filtered.blade.php | ~~showFiltered.blade.php, show_filtered.blade.php~~
|Config | snake_case | google_calendar.php | ~~googleCalendar.php, google-calendar.php~~
|Migration | - | 2017_01_01_000000_create_articles_table | ~~2017_01_01_000000_articles~~

##  For learning relationship read this blog 

[Eloquent Relationships: The Ultimate Guide](https://quickadminpanel.com/blog/eloquent-relationships-the-ultimate-guide/?fbclid=IwAR2vGb5DAGn3pS6YPNB5f7N69oWoStwXrNTDBKMffRZitPmSVH6tCpfwxL0)

[aravel.at.jeffsbox.eu](http://laravel.at.jeffsbox.eu/tag/eloquent-relationship)

## Using the unique validation rule in a Laravel Form Request
[Using the unique validation](https://www.csrhymes.com/2019/06/22/using-the-unique-validation-rule-in-laravel-form-request.html)
