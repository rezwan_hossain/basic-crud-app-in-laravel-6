<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class TodoController extends Controller
{
    public function index()
    {
    	// show all the resources
    	$todos = Todo::all();

    	return view('index', compact('todos'));
    }

    public function show($id)
    {
    	// show single resources

    	$todos = Todo::find($id);

    	return view('show', compact('todos'));
    }

    public function create()
    {
    	// show view to create new resources
    	return view('create');
    }

    public function store()
    {
    	// persist new resources
    	$todos = new Todo;
    	$todos->title = request('title');
    	$todos->description = request('description');
    	$todos->save();

    	return redirect('/');
    }

    public function edit($id)
    {
    	// show a view to edit reqources
    	$todos = Todo::find($id);

    	return view('edit', compact('todos'));
    }

    public function update($id)
    {
    	// persist edited resources
    	$todos = Todo::find($id);

    	$todos->title = request('title');
    	$todos->description = request('description');
    	$todos->save();

    	return redirect('/todos/'. $todos->id);

    }

    public function destroy($id)
    {
    	// deletee the resource
    	$todos = Todo::find($id);

    	$todos->delete();

    	return redirect('/');
    }
}
